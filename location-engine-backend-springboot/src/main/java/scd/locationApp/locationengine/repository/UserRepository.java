package scd.locationApp.locationengine.repository;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import scd.locationApp.locationengine.model.User;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    Optional<User> findByEmail(String email);


    List<User> findByIdIn(List<Long> userIds);

    Boolean existsByEmail(String email);
}