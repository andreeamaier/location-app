package scd.locationApp.locationengine.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import scd.locationApp.locationengine.model.Location;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface LocationRepository extends CrudRepository<Location, Long> {

    @Modifying
    @Query(value = "insert into locations (lat, lng, date, user_id) values (:lat,:lng,:date,:id)", nativeQuery = true)
    @Transactional
    void insert(@Param("lat") double lat, @Param("lng") double lng, @Param("date") LocalDate date, @Param("id") Long id);

    Location save(Location location);

    @Query(value = "select * from locations where date>=:startDate and date <= :endDate and user_id = :userId", nativeQuery = true)
    List<Location> findByDateIntervalAndUser(@Param("startDate") LocalDate startDate, @Param("endDate") LocalDate endDate, @Param("userId") Long userId);

    @Query(value = "select * from locations where user_id = :userId", nativeQuery = true)
    List<Location> findByUser(@Param("userId") Long userId);

    @Override
    void deleteById(Long aLong);

    @Modifying
    @Query(value = "update locations set lat = :lat, lng = :lng where id = :id", nativeQuery = true)
    @Transactional
    void updateLatAndLong(@Param("lat") double lat, @Param("lng") double lng, @Param("id") Long id);

    @Query(value = "select * from locations where id = :id", nativeQuery = true)
    Location findLocationById(@Param("id") Long id);
}
