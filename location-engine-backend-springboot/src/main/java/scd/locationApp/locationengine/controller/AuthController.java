package scd.locationApp.locationengine.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import scd.locationApp.locationengine.config.BusinessException;
import scd.locationApp.locationengine.model.dto.JwtAuthenticationResponse;
import scd.locationApp.locationengine.model.dto.UserLoginDTO;
import scd.locationApp.locationengine.model.dto.UserRegisterDTO;
import scd.locationApp.locationengine.service.UserService;

import javax.validation.Valid;

@CrossOrigin(origins = {"http://localhost:3000"})
@RestController
@RequestMapping("/api/auth")
public class AuthController {

    private static final Logger logger = LoggerFactory.getLogger(AuthController.class);

    @Autowired
    UserService userService;


    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody UserLoginDTO userLoginDTO) throws BusinessException {
        String jwt = userService.login(userLoginDTO);
        logger.info(jwt);
        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
    }


    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody UserRegisterDTO userRegisterDTO) throws BusinessException {
        userService.registerUser(userRegisterDTO);
        return ResponseEntity.ok(HttpStatus.CREATED);

    }
}