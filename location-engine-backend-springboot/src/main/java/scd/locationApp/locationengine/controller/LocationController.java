package scd.locationApp.locationengine.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import scd.locationApp.locationengine.config.BusinessException;
import scd.locationApp.locationengine.model.dto.LocationDto;
import scd.locationApp.locationengine.security.CurrentUser;
import scd.locationApp.locationengine.security.UserPrincipal;
import scd.locationApp.locationengine.service.LocationService;
import scd.locationApp.locationengine.service.LocationServiceImpl;
import scd.locationApp.locationengine.util.Constants;

import java.time.LocalDate;
import java.util.List;

@CrossOrigin(origins = {"http://localhost:3000"})
@Controller
@RequestMapping("/api/locations")
public class LocationController {

    private static final Logger LOGGER = LoggerFactory.getLogger(LocationServiceImpl.class);


    @Autowired
    private LocationService locationService;

    @PostMapping(path = "/addLocation")
    //@Secured(Constants.BASIC_USER)
    @PreAuthorize("hasRole('BASIC_USER')")
    public ResponseEntity<LocationDto> saveLocation(@CurrentUser UserPrincipal currentUser, @RequestParam String lat, @RequestParam String lng) throws BusinessException {

        LOGGER.info("Current user id = " + currentUser.getUser().getId());

        LocationDto locationDto = new LocationDto();
        locationDto.setLng(Double.parseDouble(lng));
        locationDto.setLat(Double.parseDouble(lat));

        locationService.saveLocation(locationDto, currentUser.getUser());
        return ResponseEntity.ok(locationDto);
    }

    @GetMapping(path = "/searchLocationByUserAndDate")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<List<LocationDto>> getSearchedLocations(@RequestParam(value = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
                                                                  @RequestParam(value = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate,
                                                                  @RequestParam(value = "userId") Long userId) {
        List<LocationDto> locations = locationService.searchLocationsByUserAndDate(startDate, endDate, userId);
        return ResponseEntity.ok(locations);
    }

    @GetMapping(path = "/searchLocationByUser")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<List<LocationDto>> getSearchedLocationsByUser(@RequestParam(value = "userId") Long userId) {
        List<LocationDto> locations = locationService.searchLocationsByUser(userId);
        return ResponseEntity.ok(locations);
    }

    @GetMapping("/")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity<List<LocationDto>> getAllLocations() {
        return ResponseEntity.ok(locationService.getLocations());
    }

    @DeleteMapping(path = "/{id}")
    @PreAuthorize("hasAnyRole('ADMIN','BASIC_USER')")
    public ResponseEntity deleteLocation(@PathVariable(value = "id") Long id) throws BusinessException {
        locationService.deleteLocationById(id);
        return ResponseEntity.ok(HttpStatus.NO_CONTENT);
    }

    @PutMapping(path = "/{id}")
    @PreAuthorize("hasAnyRole('ADMIN','BASIC_USER')")
    public ResponseEntity updateLatAndLong(@RequestBody final LocationDto locationDto, @PathVariable Long id) throws BusinessException {
        LOGGER.info("CONTROLLER PUT");
        locationService.updateLatAndLong(locationDto.getLat(), locationDto.getLng(), id);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @GetMapping(path = "/{id}")
    @PreAuthorize("hasAnyRole('ADMIN','BASIC_USER')")
    public ResponseEntity<LocationDto> getLocationById(@PathVariable Long id) throws BusinessException {
        return ResponseEntity.ok(locationService.getLocationById(id));
    }

}
