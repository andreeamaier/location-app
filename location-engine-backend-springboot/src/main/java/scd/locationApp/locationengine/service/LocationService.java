package scd.locationApp.locationengine.service;

import scd.locationApp.locationengine.config.BusinessException;
import scd.locationApp.locationengine.model.User;
import scd.locationApp.locationengine.model.dto.LocationDto;

import java.time.LocalDate;
import java.util.List;

public interface LocationService {

    List<LocationDto> getLocations();

    void saveLocation(LocationDto locationDto, User user) throws BusinessException;

    List<LocationDto> searchLocationsByUserAndDate(LocalDate startDate, LocalDate endDate, Long userId);

    List<LocationDto> searchLocationsByUser(Long userId);

    void deleteLocationById(Long id) throws BusinessException;

    void updateLatAndLong(double lat, double lng, long id) throws BusinessException;

    LocationDto getLocationById(Long id) throws BusinessException;
}
