package scd.locationApp.locationengine.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import scd.locationApp.locationengine.config.BusinessException;
import scd.locationApp.locationengine.model.Location;
import scd.locationApp.locationengine.model.User;
import scd.locationApp.locationengine.model.dto.LocationDto;
import scd.locationApp.locationengine.repository.LocationRepository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class LocationServiceImpl implements LocationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LocationServiceImpl.class);

    @Autowired
    private LocationRepository locationRepository;

    @Override
    public List<LocationDto> getLocations() {
        List<Location> locations = (List<Location>) locationRepository.findAll();
        return mapListFromEntityToDto(locations);
    }

    @Override
    @Transactional
    public void saveLocation(LocationDto locationDto, User user) throws BusinessException {

        if (Objects.isNull(locationDto)) {
            throw new BusinessException(401, "Body null !");
        }

        if (!isLongitudeValid(locationDto.getLng())) {
            throw new BusinessException(400, "Longitude is not correct.");
        }

        if (!isLatitudeValid(locationDto.getLat())) {
            throw new BusinessException(400, "Latitude is not correct.");
        }

        LOGGER.info(locationDto.getLat() + " " + locationDto.getLng() + " " + LocalDate.now() + " " + user.getId());
        locationRepository.insert(locationDto.getLat(), locationDto.getLng(), LocalDate.now(), user.getId());
    }

    @Override
    @Transactional
    public List<LocationDto> searchLocationsByUserAndDate(LocalDate startDate, LocalDate endDate, Long userId) {
        List<Location> locations = locationRepository.findByDateIntervalAndUser(startDate, endDate, userId);
        return mapListFromEntityToDto(locations);
    }

    @Override
    @Transactional
    public List<LocationDto> searchLocationsByUser(Long userId) {
        List<Location> locations = locationRepository.findByUser(userId);
        return mapListFromEntityToDto(locations);
    }

    @Override
    @Transactional
    public void deleteLocationById(Long id) throws BusinessException {
        Location location = locationRepository.findLocationById(id);
        if (Objects.isNull(location)) {
            throw new BusinessException(404, "Location not found");
        }
        locationRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void updateLatAndLong(double lat, double lng, long id) throws BusinessException {
        Location location = locationRepository.findLocationById(id);

        if (Objects.isNull(location)) {
            throw new BusinessException(404, "Location not found");
        }

        if (!isLongitudeValid(lng)) {
            throw new BusinessException(400, "Longitude is not correct.");
        }

        if (!isLatitudeValid(lat)) {
            throw new BusinessException(400, "Latitude is not correct.");
        }
        locationRepository.updateLatAndLong(lat, lng, id);
    }

    @Override
    @Transactional
    public LocationDto getLocationById(Long id) throws BusinessException {
        Location location = locationRepository.findLocationById(id);
        if (Objects.isNull(location)) {
            throw new BusinessException(404, "Location not found");
        }
        return mapFromEntityToDto(location);
    }

    private List<LocationDto> mapListFromEntityToDto(List<Location> locations) {
        List<LocationDto> locationDtos = new ArrayList<>();
        for (Location l : locations) {
            LocationDto locationDto = new LocationDto();
            locationDto.setLat(l.getLat());
            locationDto.setLng(l.getLng());
            locationDtos.add(locationDto);
        }

        return locationDtos;
    }

    private LocationDto mapFromEntityToDto(Location location) {
        LocationDto locationDto = new LocationDto();
        locationDto.setLat(location.getLat());
        locationDto.setLng(location.getLng());

        return locationDto;
    }

    private boolean isLongitudeValid(double longitude) {
        return (longitude >= -180 && longitude <= 180);
    }

    private boolean isLatitudeValid(double latitude) {
        return (latitude >= -90 && latitude <= 90);
    }

}
