package scd.locationApp.locationengine.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import scd.locationApp.locationengine.config.BusinessException;
import scd.locationApp.locationengine.model.Role;
import scd.locationApp.locationengine.model.User;
import scd.locationApp.locationengine.model.dto.UserDTO;
import scd.locationApp.locationengine.model.dto.UserLoginDTO;
import scd.locationApp.locationengine.model.dto.UserRegisterDTO;
import scd.locationApp.locationengine.repository.RoleRepository;
import scd.locationApp.locationengine.repository.UserRepository;
import scd.locationApp.locationengine.security.JwtTokenProvider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Component
public class UserServiceImpl implements UserService {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JwtTokenProvider tokenProvider;

    @Override
    @Transactional
    public List<UserDTO> getAllUsers() {
        List<User> users = (List<User>) userRepository.findAll();
        List<UserDTO> userDTOS = new ArrayList<>();
        for (User user : users) {
            UserDTO userDTO = mapUserEntityToDto(user);
            userDTOS.add(userDTO);
        }
        return userDTOS;
    }

    @Override
    @Transactional
    public boolean existsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    @Override
    @Transactional
    public UserDTO findUserById(Long id) throws BusinessException {
        User user = userRepository.findById(id)
                .orElseThrow(() -> new BusinessException(404, "User not found"));
        return mapUserEntityToDto(user);
    }

    @Override
    @Transactional
    public void registerUser(UserRegisterDTO userRegisterDTO) throws BusinessException {

        if (existsByEmail(userRegisterDTO.getEmail())) {
            throw new BusinessException(400, "Email address already taken.");
        }
        if (Objects.isNull(userRegisterDTO)) {
            throw new BusinessException(401, "Body cannot be null!");
        }
        if (Objects.isNull(userRegisterDTO.getEmail())|| userRegisterDTO.getEmail().equals("")) {
            throw new BusinessException(400, "Email cannot be null ! ");
        }
        if (Objects.isNull(userRegisterDTO.getPassword()) || userRegisterDTO.getPassword().equals("")) {
            throw new BusinessException(400, "Password cannot be null !");
        }
        if (Objects.isNull(userRegisterDTO.getFirstName())|| userRegisterDTO.getFirstName().equals("")) {
            throw new BusinessException(400, "First name cannot be null ! ");
        }
        if (Objects.isNull(userRegisterDTO.getLastName()) || userRegisterDTO.getLastName().equals("")) {
            throw new BusinessException(400, "Last name cannot be null !");
        }

        User user = new User(userRegisterDTO.getFirstName(), userRegisterDTO.getLastName(),
                userRegisterDTO.getEmail(), userRegisterDTO.getPassword());
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        Role userRole = roleRepository.findByRole("BASIC_USER")
                .orElseThrow(() -> new BusinessException(403, "User Role not set."));
        user.setRoles(Collections.singleton(userRole));
        userRepository.save(user);
    }

    @Override
    @Transactional
    public String login(UserLoginDTO userLoginDTO) throws BusinessException {

        if (Objects.isNull(userLoginDTO)) {
            throw new BusinessException(401, "Body cannot be null!");
        }
        if (Objects.isNull(userLoginDTO.getEmail()) || userLoginDTO.getEmail().equals("")) {
            throw new BusinessException(400, "Email cannot be null! ");
        }
        if (Objects.isNull(userLoginDTO.getPassword()) || userLoginDTO.getPassword().equals("")) {
            throw new BusinessException(400, "Password cannot be null!");
        }

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        userLoginDTO.getEmail(),
                        userLoginDTO.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return tokenProvider.generateToken(authentication);
    }

    private UserDTO mapUserEntityToDto(User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setFirstName(user.getFirstName());
        userDTO.setLastName(user.getLastName());
        userDTO.setEmail(user.getEmail());
        return userDTO;
    }
}
