package scd.locationApp.locationengine.service;

import org.springframework.stereotype.Service;
import scd.locationApp.locationengine.config.BusinessException;
import scd.locationApp.locationengine.model.dto.UserDTO;
import scd.locationApp.locationengine.model.dto.UserLoginDTO;
import scd.locationApp.locationengine.model.dto.UserRegisterDTO;

import java.util.List;

@Service
public interface UserService {
    List<UserDTO> getAllUsers();

    boolean existsByEmail(String email);

    UserDTO findUserById(Long id) throws BusinessException;

    void registerUser(UserRegisterDTO userRegisterDTO) throws BusinessException;

    String login(UserLoginDTO userLoginDTO) throws BusinessException;
}
