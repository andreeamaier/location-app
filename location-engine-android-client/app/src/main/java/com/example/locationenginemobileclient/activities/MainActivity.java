package com.example.locationenginemobileclient.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.locationenginemobileclient.R;
import com.example.locationenginemobileclient.service.utils.RetrofitClientInstance;
import com.example.locationenginemobileclient.service.utils.UserClient;
import com.example.locationenginemobileclient.model.Login;
import com.example.locationenginemobileclient.model.LoginResponse;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    public static final String TOKEN_STR = "token";

    String token;
    Button loginBtn;
    Button registerBtn;
    TextView email;
    TextView password;

    UserClient userClient = RetrofitClientInstance.getRetrofitInstance()
            .create(UserClient.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loginBtn = findViewById(R.id.loginBtn);
        registerBtn = findViewById(R.id.registerBtn);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);


        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });

        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register();
            }
        });

    }

    private void register() {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    private void login() {
        Login login = new Login(email.getText().toString(), password.getText().toString());
        Call<LoginResponse> call = userClient.login(login);

        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()){
                    token = response.body().getAccessToken();
                    Intent intent = new Intent(MainActivity.this, LocationActivity2.class);
                    intent.putExtra(TOKEN_STR, token);
                    startActivity(intent);
                } else {
                    Toast.makeText(MainActivity.this, "Bad credentials", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Error!", Toast.LENGTH_SHORT).show();
            }
        });
    }


}
