package com.example.locationenginemobileclient.service.utils;

import com.example.locationenginemobileclient.model.Coordinates;
import com.example.locationenginemobileclient.model.Login;
import com.example.locationenginemobileclient.model.LoginResponse;
import com.example.locationenginemobileclient.model.Register;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface UserClient {

    @POST("auth/signin")
    Call<LoginResponse> login(@Body Login login);

    @POST("auth/signup")
    Call<String> register(@Body Register register);

    @POST("locations/addLocation")
    Call<Coordinates> sendLocation(@Header("Authorization") String authToke, @Query("lat") String lat, @Query("lng") String lng);
}
