package com.example.locationenginemobileclient.activities;

import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.locationenginemobileclient.R;
import com.example.locationenginemobileclient.model.Register;
import com.example.locationenginemobileclient.model.RegisterResponse;
import com.example.locationenginemobileclient.service.utils.RetrofitClientInstance;
import com.example.locationenginemobileclient.service.utils.UserClient;

public class RegisterActivity extends AppCompatActivity {

    TextView firstName, lastName, email, pass;
    Button registerBtn;

    UserClient userClient = RetrofitClientInstance.getRetrofitInstance()
            .create(UserClient.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register2);

        firstName = findViewById(R.id.firstName);
        lastName = findViewById(R.id.lastName);
        email = findViewById(R.id.emailReg);
        pass = findViewById(R.id.passwordReg);

        registerBtn = findViewById(R.id.reg);

        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register();
            }
        });
    }

    private void register() {
        Register register = new Register();
        register.setFirstName(firstName.getText().toString());
        register.setLastName(lastName.getText().toString());
        register.setEmail(email.getText().toString());
        register.setPassword(pass.getText().toString());

        Call<String> call = userClient.register(register);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()){
                    Toast.makeText(RegisterActivity.this, "Account created. Please login.", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(RegisterActivity.this, "Failed registration", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(RegisterActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
