package com.example.locationenginemobileclient.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.locationenginemobileclient.R;
import com.example.locationenginemobileclient.model.Coordinates;
import com.example.locationenginemobileclient.service.utils.RetrofitClientInstance;
import com.example.locationenginemobileclient.service.utils.UserClient;

import java.io.IOException;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LocationActivity2 extends AppCompatActivity implements LocationListener {

    String token;
    TextView text;
    Button sendButton, sendButtonContinue, stopButton;
    LocationManager locationManager;
    private Handler handler = new Handler();
    Runnable runnable;

    String lat;
    String lng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location2);

        Intent intent = getIntent();
        token = intent.getStringExtra(MainActivity.TOKEN_STR);


        sendButton = findViewById(R.id.button_send);
        sendButtonContinue = findViewById(R.id.button_send_continue);
        stopButton = findViewById(R.id.button_stop);
        text = findViewById(R.id.textView);

        runnable = new Runnable() {
            @Override
            public void run() {
                getLocation();
                if (lat != null && lng != null) {
                    send(lat, lng);
                }
                handler.postDelayed(this,10*1000);
            }
        };

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(LocationActivity2.this, "Location sent.", Toast.LENGTH_SHORT).show();
                getLocation();
                if (lat != null && lng != null) {
                    send(lat, lng);
                }
            }
        });

        sendButtonContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(LocationActivity2.this, "Send location continue activated.", Toast.LENGTH_SHORT).show();

                sendButton.setClickable(false);
                sendButtonContinue.setClickable(false);
                stopButton.setVisibility(View.VISIBLE);
                handler.postDelayed(runnable, 10 * 1000);
            }
        });

        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.removeCallbacksAndMessages(runnable);
                handler.removeCallbacks(runnable);

                stopButton.setVisibility(View.INVISIBLE);
                sendButton.setClickable(true);
                sendButtonContinue.setClickable(true);
            }
        });
    }

    private void send(String latitude, String longitude) {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request request = chain.request().newBuilder().addHeader("parameter", "value").build();
                return chain.proceed(request);
            }
        });

        UserClient userClient = RetrofitClientInstance.getRetrofitInstance()
                .create(UserClient.class);


        Call<Coordinates> call = userClient.sendLocation("Bearer " + token, latitude, longitude);
        call.enqueue(new Callback<Coordinates>() {
            @Override
            public void onResponse(Call<Coordinates> call, Response<Coordinates> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(LocationActivity2.this, response.body().getLatitude() + " " + response.body().getLongitude(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(LocationActivity2.this, "FAILED SENDING LOCATION" + response.body(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Coordinates> call, Throwable t) {
                Toast.makeText(LocationActivity2.this, "ERROR", Toast.LENGTH_SHORT).show();
            }
        });
    }

    void getLocation() {
        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.INTERNET}, 1);
                return;
            } else {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 0, LocationActivity2.this);
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        text.setText("Current location: \n" + location.getLatitude() + ", \n" + location.getLongitude());
        setLocation(Double.toString(location.getLatitude()), Double.toString(location.getLongitude()));
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(LocationActivity2.this, "Please Enable GPS and Internet", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }

    private void setLocation(String lat, String lng) {
        this.lat = lat;
        this.lng = lng;
    }
}
