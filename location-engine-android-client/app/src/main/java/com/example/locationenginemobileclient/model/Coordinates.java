package com.example.locationenginemobileclient.model;

import com.google.gson.annotations.SerializedName;

public class Coordinates {

    @SerializedName("lng")
    private String longitude;

    @SerializedName("lat")
    private String latitude;

    public Coordinates(String longitude, String latitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }
}
